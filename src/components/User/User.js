import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../../store/actions/index';
import LoadingPage from '../LoadingPage/LoadingPage';
import Profile from './Profile/Profile'

class User extends Component {
    async componentDidMount () {
        await this.props.onGetUser(this.props.match.params.id)
    }

    handleClickBack = () => {
        console.log(this.props.history);
        this.props.history.goBack();
    }

    render() {
        if( this.props.isLoading ){
            return <LoadingPage />
        }
        if( this.props.error !== null ){
            return <p>[Error]: {this.props.error}</p>
        }

        let fname = null;
        let lname = null;
        let avatar = null;
        let email = null;
        let id = null;

        if( this.props.user !== null ){
            fname = this.props.user.first_name;
            lname = this.props.user.last_name;
            avatar = this.props.user.avatar;
            email = this.props.user.email;
            id = this.props.user.id;
        }

        return (
            <Profile 
                id={id}
                fname={fname}
                lname={lname}
                avatar={avatar}
                email={email}
                clicked={this.handleClickBack}
                 />
        );
    }
}

const mapStateToProps = state => {
    return {
        user: state.user.user,
        isLoading: state.user.isLoading,
        error: state.user.error,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onGetUser: (id) => dispatch( actions.getUser(id) ),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(User);