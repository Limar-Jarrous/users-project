import React from 'react';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import './Profile.css';


const userProfile = (props) => {
    return (
        <div className='profile_box'>
            <button className='back-btn'
                    onClick={props.clicked}>
                <ArrowBackIcon className='arrow-icon'/>
            </button>
            <div className='profile_cover'></div>
            <div className='profile_info'>
                <h1>{props.fname + ' ' + props.lname}</h1>
                <h4>{props.email}</h4>
                <p>
                    <strong>About me:</strong> <br/><br/>
                    Duis lectus nec scelerisque est? Orci aliquam nisl potenti per fermentum sociosqu venenatis. 
                    Mauris tellus litora facilisis aptent himenaeos suscipit donec turpis. 
                    Nulla sociosqu dis arcu nostra. In aliquet torquent tellus diam justo nascetur lectus imperdiet.</p>
            </div>
            <div className='img'>
                <img src={props.avatar} alt='personal_photo' />
            </div>
        </div>
    );
}

export default userProfile;