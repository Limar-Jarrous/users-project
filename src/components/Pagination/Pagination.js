import React, { Component } from 'react';
import { connect } from 'react-redux';
import Pagination from '@material-ui/lab/Pagination';
import './Pagination.css';
import { withRouter } from 'react-router';


class PaginationControlled extends Component {
	handleChange = (event, value) => {
		if(this.props.pageNum !== value){
			this.props.history.push(`/users/page=${value}`)
		}
	};

	render(){
		return (
			<div className='Controls'>
				<Pagination 
					count={this.props.totalPages} 
					page={this.props.pageNum}
					onChange={this.handleChange} />
			</div>
		);
	}
}

const mapStateToProps = state => {
	return {
		totalPages: state.users.totalPages,
	}
}

export default withRouter(connect(mapStateToProps)(PaginationControlled));