import React, { Component } from 'react';
import Spinner from '../Spinner/Spinner';
import './LoadingPage.css';


class LoadingPage extends Component {    
    render() { 
        return (
            <div className='Body'>
                <div className='Spinner'>
                    <Spinner />
                </div>
            </div>
        );
    }
}

export default LoadingPage;