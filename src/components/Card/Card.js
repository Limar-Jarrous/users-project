import React, {Component} from 'react';
import './Card.css';
import { withRouter } from 'react-router';

class Card extends Component {
    render () {
        const user = this.props;
        return (
            // <div className='UserProfile' onClick={(event) => this.props.clicked(event, user.id)}>
            <div className='UserProfile' onClick={(event) => this.props.history.push(`/user/${user.id}`)}>
                <img src={user.avatar} alt='userImg' />
                <div className='Info'>
                    <div className='Name'>{user.first_name + ' ' + user.last_name}</div>
                    <div className='Details'>{user.email}</div>
                </div>
            </div>
        )
    }
}

export default withRouter(Card);