import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../../store/actions/index';
import './Header.css';

class Header extends Component {
    state = {
        user: '',
    }

    handleUserChanged = (event) => {
        this.setState({user: event.target.value});
        this.props.onSearchChange(event.target.value);        
    }
    
    render(){
        return (
            <header className='header'>
                <a href='/'>Home Logo</a>
                <input 
                    type='text' 
                    value={this.state.user}
                    placeholder='Search users by name'
                    onChange={(event) => this.handleUserChanged(event)} />
            </header>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onSearchChange: (search_text) => dispatch(actions.changeSearch(search_text)),
    }
}

export default connect(null, mapDispatchToProps)(Header);