import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../../store/actions/index';

import CardList from '../CardList/CardList';
import LoadingPage from '../LoadingPage/LoadingPage';
import './Users.css';


class Users extends Component {
    state = {
        pageNum: parseInt(this.props.match.params.pageNum),
    }
    
    getUsersData(pageNumber){
        this.props.onGetUsers(pageNumber);
    }

    componentDidMount(){
        this.getUsersData(this.props.match.params.pageNum);
    }

    componentDidUpdate(prevProps) {
        // console.log('old props => ', prevProps.match.params.pageNum);
        // console.log('new props => ', this.props.match.params.pageNum);
        // console.log('state => ', this.state.pageNum);
        if (this.props.match.params.pageNum !== prevProps.match.params.pageNum) {
            this.setState({pageNum: parseInt(this.props.match.params.pageNum)})
            this.getUsersData(this.props.match.params.pageNum);
        }
    }

    render() {
        if( this.props.isLoading ){
            return <LoadingPage />
        }
        if( this.props.error !== null ){
            return <p>[Error]: {this.props.error}</p>
        }
        return (
                <div className='container'>
                    <CardList 
                        users={this.props.users}
                        totalPages={this.props.totalPages}
                        searchText={this.props.searchText}
                        pageNum={this.state.pageNum}
                    />
                </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        users: state.users.users,
        isLoading: state.users.isLoading,
        error: state.users.error,
        totalPages: state.users.totalPages,
        searchText: state.search.search_text,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onGetUsers: (pageNum) => dispatch(actions.getUsers(pageNum)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Users);