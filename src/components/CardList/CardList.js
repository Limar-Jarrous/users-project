import React, {Component} from 'react';
import Card from '../Card/Card';
import Pagination from '../Pagination/Pagination';
import './CardList.css';

class CardList extends Component {

    render () {
        
        const searchText = this.props.searchText;
        const allUsers = this.props.users;
        
        let filteredUsers = allUsers.filter(user => (
            user.id.toString().includes(searchText) 
        ));
                
        return (
            <div className='box'>
                <div className='profiles'>
                    {filteredUsers.map( user => {
                        return <Card 
                                    key={user.id}
                                    {...user}
                                />
                    } )}
                </div>
                {this.props.searchText
                    ? null
                    : <Pagination 
                            totalPages={this.props.totalPages}
                            pageNum={this.props.pageNum} 
                        />
                }
            </div>
        )
    }
}

export default CardList;