import * as actionTypes from '../actions/actionTypes';

const initialState = {
    users: [],
    isLoading: false,
    error: null,
    totalPages: null,
}

const handleGetUsersStart = (state, action) => {
    return {
        ...state,
        users: [],
        isLoading: true,
        error: null,
        totalPages: null,
    }
}

const handleGetUsersSuccess = (state, action) => {
    return {
        ...state,
        users: action.users,
        isLoading: false,
        error: null,
        totalPages: action.totalPages,
    }
}

const handleGetUsersFail = (state, action) => {
    return {
        ...state,
        users: [],
        isLoading: false,
        error: action.error,
        totalPages: null,
    }
}

const usersReducer = (state=initialState, action) => {
    switch(action.type){
        case actionTypes.GET_USERS_START: return handleGetUsersStart(state, action);
        case actionTypes.GET_USERS_SUCCESS: return handleGetUsersSuccess(state, action);
        case actionTypes.GET_USERS_FAIL: return handleGetUsersFail(state, action);
        default: return state;
    }
}

export default usersReducer;