import * as actionTypes from '../actions/actionTypes';

const initialState = {
    search_text: '',
}

const handleChangeSearchText = (state, action) => {
    return {
        ...state,
        search_text: action.search_text,
    }
}

const searchReducer = (state=initialState, action) => {
    switch(action.type){
        case actionTypes.CHANGE_SEARCH_TEXT: return handleChangeSearchText(state, action);
        default: return state;
    }
}

export default searchReducer;