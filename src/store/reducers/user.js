import * as actionTypes from '../actions/actionTypes';

const initialState = {
    user: null,
    isLoading: false,
    error: null
}

const handleGetUserStart = (state, action) => {
    return {
        ...state,
        user: null,
        isLoading: true,
        error: null
    }
}

const handleGetUserSuccess = (state, action) => {
    return {
        ...state,
        user: action.user,
        isLoading: false,
        error: null
    }
}

const handleGetUserFail = (state, action) => {
    return {
        ...state,
        user: null,
        isLoading: false,
        error: action.error
    }
}

export const userReducer = (state=initialState, action) => {
    switch(action.type){
        case actionTypes.GET_USER_START: return handleGetUserStart(state, action);
        case actionTypes.GET_USER_SUCCESS: return handleGetUserSuccess(state, action);
        case actionTypes.GET_USER_FAIL: return handleGetUserFail(state, action);
        default: return state;
    }
}

export default userReducer;