import { combineReducers } from 'redux';

import usersReducer from './users';
import userReducer from './user';
import searchReducer from './search';

export default combineReducers({
    users: usersReducer,
    user: userReducer,
    search: searchReducer,
});
