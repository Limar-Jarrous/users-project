import * as actionTypes from './actionTypes';

export const handleChangeSearch = (search_text) => ({
    type: actionTypes.CHANGE_SEARCH_TEXT,
    search_text: search_text,
})

export const changeSearch = (search_text) => {
    return (dispatch) => {
        dispatch( handleChangeSearch(search_text) );
    }
}