import * as actionTypes from './actionTypes';
import axios from 'axios';

export const getUsersStart = () => ({
    type: actionTypes.GET_USERS_START,
})

export const getUsersSuccess = (Data) => ({
    type: actionTypes.GET_USERS_SUCCESS,
    users: Data.data,
    totalPages: Data.total_pages,
})

export const getUsersFail = (error) => ({
    type: actionTypes.GET_USERS_FAIL,
    error: error
})

export const getUsers = (pageNum) => {
    return (dispatch) => {
        dispatch( getUsersStart() );
        return axios.get(`https://reqres.in/api/users?page=${pageNum}`)
                    .then( resp => {
                        dispatch( getUsersSuccess(resp.data) );
                    })
                    .catch( err => {
                        dispatch( getUsersFail(err.message) );
                    })
    }
}