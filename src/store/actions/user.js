import * as actionTypes from './actionTypes';
import axios from 'axios';

export const getUserStart = () => ({
    type: actionTypes.GET_USER_START,
})

export const getUserSuccess = (user) => ({
    type: actionTypes.GET_USER_SUCCESS,
    user: user.data,
})

export const getUserFail = (error) => ({
    type: actionTypes.GET_USER_FAIL,
    error: error
})

export const getUser = (userId) => {
    return (dispatch) => {
        dispatch( getUserStart() );
        return axios.get(`https://reqres.in/api/users/${userId}`)
                    .then( resp => {
                        dispatch( getUserSuccess(resp.data) );
                    })
                    .catch( err => {
                        dispatch( getUserFail(err.message) );
                    })
    }
}