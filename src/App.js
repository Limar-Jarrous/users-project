import React, {Component} from 'react';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import Users from './components/Users/Users';
import User from './components/User/User';
import Header from './components/Header/Header';


class App extends Component {
  render () {
    return (
      <BrowserRouter>
        <Header />
        <Switch>
            <Route exact path='/user/:id' component={User}/>
            <Route exact path='/users/page=:pageNum' component={Users}/>
            <Redirect to='/users/page=1'/>
        </Switch>
      </BrowserRouter>
    );
  }
}

export default App;